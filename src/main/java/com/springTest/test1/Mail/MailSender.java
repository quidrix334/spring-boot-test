package com.springTest.test1.Mail;

public interface MailSender {

    void send(String to, String subject, String body);
}
