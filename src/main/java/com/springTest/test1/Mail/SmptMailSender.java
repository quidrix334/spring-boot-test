package com.springTest.test1.Mail;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("smtp")
@Qualifier("smtpMail")
public class SmptMailSender implements MailSender{

    private static Log log = LogFactory.getLog(SmptMailSender.class);

    @Override
    public void send(String to, String subject, String body) {
            log.info("Sending SMTP mail to "+ to);
            log.info("with subject: " +subject);
            log.info("and body: " + body);
    }
}
