package com.springTest.test1.Controllers;

import com.springTest.test1.Mail.MailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MailController {

    private MailSender mailSender;

    public MailController(@Qualifier("smtpMail") MailSender smtp) {
        this.mailSender = smtp;
    }

    @RequestMapping("/mail")
    public String mail(){
        mailSender.send("example@mail.com","Testing","This is just an example");
        return "Mail send";
    }
}
